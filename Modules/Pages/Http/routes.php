<?php

Route::group([
    'middleware' => 'web',
    'prefix' => config('shop.admin.url'),
    'namespace' => 'LaraShop\Pages\Http\Controllers'
], function()
{
    Route::get('/', 'DashboardController@index');
});
