<?php

namespace LaraShop\Products\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'products_categories';

    protected $fillable = [];
}
