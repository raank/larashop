<?php

namespace LaraShop\Products\Entities;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'products_tags';

    protected $fillable = [];

    public $timestamp = false;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_tags_relations', 'tag_id', 'product_id');

    }
}
