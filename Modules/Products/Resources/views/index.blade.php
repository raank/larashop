@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Traffic &amp; Sales
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="callout callout-info">
                                        <small class="text-muted">New Clients</small>
                                        <br>
                                        <strong class="h4">9,123</strong>
                                        <div class="chart-wrapper">
                                            <canvas id="sparkline-chart-1" width="100" height="30"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <!--/.col-->
                                <div class="col-sm-6">
                                    <div class="callout callout-danger">
                                        <small class="text-muted">Recuring Clients</small>
                                        <br>
                                        <strong class="h4">22,643</strong>
                                        <div class="chart-wrapper">
                                            <canvas id="sparkline-chart-2" width="100" height="30"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <!--/.col-->
                            </div>
                            <!--/.row-->
                            <hr class="mt-0">
                        </div>
                        <!--/.col-->
                        <div class="col-sm-6 col-lg-4">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="callout callout-warning">
                                        <small class="text-muted">Pageviews</small>
                                        <br>
                                        <strong class="h4">78,623</strong>
                                        <div class="chart-wrapper">
                                            <canvas id="sparkline-chart-3" width="100" height="30"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <!--/.col-->
                                <div class="col-sm-6">
                                    <div class="callout callout-success">
                                        <small class="text-muted">Organic</small>
                                        <br>
                                        <strong class="h4">49,123</strong>
                                        <div class="chart-wrapper">
                                            <canvas id="sparkline-chart-4" width="100" height="30"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <!--/.col-->
                            </div>
                            <!--/.row-->
                            <hr class="mt-0">
                        </div>
                        <!--/.col-->
                        <div class="col-sm-6 col-lg-4">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="callout">
                                        <small class="text-muted">CTR</small>
                                        <br>
                                        <strong class="h4">23%</strong>
                                        <div class="chart-wrapper">
                                            <canvas id="sparkline-chart-5" width="100" height="30"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <!--/.col-->
                                <div class="col-sm-6">
                                    <div class="callout callout-primary">
                                        <small class="text-muted">Bounce Rate</small>
                                        <br>
                                        <strong class="h4">5%</strong>
                                        <div class="chart-wrapper">
                                            <canvas id="sparkline-chart-6" width="100" height="30"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <!--/.col-->
                            </div>
                            <!--/.row-->
                            <hr class="mt-0">
                        </div>
                        <!--/.col-->
                    </div>
                    <!--/.row-->
                    <br>
                    <table class="table table-responsive table-hover table-outline mb-0">
                        <thead class="thead-default">
                            <tr>
                                <th class="text-center"><i class="fa fa-hashtag"></i></th>
                                <th>Produto</th>
                                <th class="text-center">Country</th>
                                <th>Estoque</th>
                                <th class="text-center">Payment Method</th>
                                <th>Activity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">
                                    <span>1651651</span>
                                </td>
                                <td>
                                    <div>Yiorgos Avraamu</div>
                                    <div class="small text-muted">
                                        <span>R$ 10,00</span> | 4/8
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="img/flags/USA.png" alt="USA" style="height:24px;">
                                </td>
                                <td>
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <strong>5</strong>
                                        </div>
                                        <div class="float-right">
                                            <small class="text-muted">3 vendidos hoje</small>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <i class="fa fa-cc-mastercard" style="font-size:24px"></i>
                                </td>
                                <td>
                                    <div class="small text-muted">Last login</div>
                                    <strong>10 sec ago</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
    <!--/.row-->
@endsection