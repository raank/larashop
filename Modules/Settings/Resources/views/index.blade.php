@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-sm-6">

			<div class="card">
				<div class="card-header">
					<strong>Geral</strong>
					<small>Configurações do Site</small>
				</div>
				<div class="card-block">
					@foreach( $settings as $setting )
						@if( $setting->subarea == 'site' )
							<div class="form-group">
								<label for="{{ $setting->subarea . '-' . $setting->key }}">{{ $setting->description->title }}</label>
								<input type="text" class="form-control" id="{{ $setting->subarea . '-' . $setting->key }}" name="{{ $setting->subarea . '-' . $setting->key }}" placeholder="{{ $setting->observations }}" value="{{ ( isset( $setting->value ) ? $setting->value : '' ) }}">
							</div>
						@endif
					@endforeach
				</div>
			</div>

		</div>
		<!--/.col-->

		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<strong>Dados do PagSeguro</strong>
				</div>
				<div class="card-block">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="pagseguro_email">E-mail</label>
								<input type="email" class="form-control" id="pagseguro_email" name="pagseguro_email">
							</div>
						</div>
					</div>
					<!--/.row-->

					<div class="row">
						<div class="col-sm-12">
							<div class="form-group" style="margin-bottom: 0;">
								<label for="pagseguro_token">Token</label>
								<input type="text" class="form-control" id="pagseguro_token">
							</div>
						</div>
					</div>
					<!--/.row-->

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <h5>SandBox <small>(testes)</small></h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="pagseguro_token">Token</label>
                                <div class="radio">
                                    <label class="radio-inline" for="pagseguro_sandbox_true">
                                        <input type="radio" id="pagseguro_sandbox_true" name="pagseguro_sandbox" value="1"> Ativado
                                    </label>
                                    <label class="radio-inline" for="pagseguro_sandbox_false">
                                        <input type="radio" id="pagseguro_sandbox_false" name="pagseguro_sandbox" value="0"> Desativado
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="pagseguro_email">E-mail</label>
                                <input type="email" class="form-control" id="pagseguro_email" name="pagseguro_email">
                            </div>
                        </div>
                    </div>
                    <!--/.row-->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="pagseguro_token">Token</label>
                                <input type="text" class="form-control" id="pagseguro_token">
                            </div>
                        </div>
                    </div>
                    <!--/.row-->
				</div>
			</div>

		</div>
		<!--/.col-->
	</div>
	<!--/.row-->
@stop
