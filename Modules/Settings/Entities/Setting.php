<?php

namespace LaraShop\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    public $timestamp = false;


    /*
     * Descrition of Settings
     */
    public function description()
    {
    	return $this->hasOne( Description::class );
    }
}
