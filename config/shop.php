<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Shop Configuration
    |--------------------------------------------------------------------------
    */

    'admin' => [
        'url' => 'admin'
    ],

    'account' => [
        'profile' => 'perfil',
        'shopping' => 'compras',
        ''
    ],

    'site' => [
        'name' => 'Teste',
        'pica' => 'minha'
    ]
];