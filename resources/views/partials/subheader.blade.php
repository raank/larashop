<!-- Breadcrumb -->
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item"><a href="#">Admin</a>
    </li>
    <li class="breadcrumb-item active">Dashboard</li>

    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
        <div class="btn-group" role="group">
            <a class="btn btn-secondary" href="{{ System::url_admin('messages') }}"><i class="icon-speech"></i></a>
            <a class="btn btn-secondary" href="{{ System::url_admin() }}"><i class="icon-graph"></i> &nbsp;Dashboard</a>
            <a class="btn btn-secondary" href="{{ System::url_admin('settings') }}"><i class="icon-settings"></i> &nbsp;Settings</a>
        </div>
    </li>
</ol>