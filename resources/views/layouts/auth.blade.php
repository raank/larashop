<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Icons -->
    <link href="{{ asset('components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('components/simple-line-icons/css/simple-line-icons.css') }}"
          rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{ asset('dist/css/style.min.css') }}" rel="stylesheet">
</head>
<body class="app flex-row align-items-center">
    @yield('content')

    <!-- Bootstrap and necessary plugins -->
    <script src="{{ asset('components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('components/tether/dist/js/tether.min.js') }}"></script>
    <script src="{{ asset('components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
</body>
</html>
