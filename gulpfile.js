'use strict';

var gulp    = require( 'gulp' );
var loadPlugins = require( 'gulp-load-plugins' );
var fs      = require( 'fs' );
var argv    = require( 'minimist' )(process.argv.slice(1));
var path    = require( 'path' );
var plugins = loadPlugins({
    lazy: false,
    rename: {
        'gulp-autoprefixer': 'prefix',
        'gulp-minify-css': 'minifyCss',
        'gulp-webserver': 'webServer'
    }
});

/*
 * Front tasks
 * ========================================
 */
gulp.task('clean:scripts', function () {
    return gulp.src( './public/dist/js/**/*.js', {read: false})
        .pipe(plugins.clean());
});

gulp.task('clean:styles', function () {
    return gulp.src( './public/dist/css/**/*.css', {read: false})
        .pipe(plugins.clean());
});

gulp.task( 'scripts', function() {
    return gulp.src(['./resources/assets/js/**/*.js', '!' + './resources/assets/js/**/_*.js', '!' + './resources/assets/js/**/*.min.js'])
        .pipe( plugins.plumber({errorHandler: plugins.notify.onError("Erro ao compilar os scripts, <%= error.message %>")}) )
        .pipe( plugins.dynamic({
            paths: [ './public/components' ]
        }))
        .pipe( plugins.if( !( argv.d || argv.debug ), plugins.uglify() ) )
        .pipe( plugins.rename({
            suffix: '.min'
        }))
        .pipe( plugins.rimraf( './public/dist/js/*' ) )
        .pipe( gulp.dest( './public/dist/js' ) )
        .pipe( plugins.if(argv.n || argv.notify, plugins.notify("JS Compilado: <%= file.relative %>")) )
});

gulp.task('styles', function () {
    return plugins.rubySass('./resources/assets/sass/**/*.scss', {
        noCache      : true,
        precision    : 6,
        style: 'compressed',
        loadPath: [ '.' ]
    })
        .pipe( plugins.prefix() )
        .pipe( plugins.minifyCss() )
        .pipe( plugins.rename({
            suffix: '.min'
        }))
        .pipe( gulp.dest('./public/dist/css') )
        .pipe( plugins.if(argv.n || argv.notify, plugins.notify("CSS Compilado: <%= file.relative %>")) )
});

gulp.task( 'watch', function() {
    gulp.watch([ './resources/assets/js/**/*.js' ], ['scripts']);
    gulp.watch([ './resources/assets/sass/**/*.scss' ], ['styles']);
});

gulp.task('build', ['clean:scripts', 'clean:styles', 'scripts', 'styles']);

gulp.task('webserver', function() {
    gulp.src('./')
        .pipe(plugins.webServer({
            path: './',
            livereload: true,
            fallback: 'index.html'
        }));
});

/*
 * Default Task
 * ========================================
 */
gulp.task('default', ['build', 'watch']);
